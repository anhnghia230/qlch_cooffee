<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Timekeeping extends Model
{
    use HasFactory;

    protected $fillable = [
        "staff_id",
        "Checkin",
        "Checkout",
    ];

    public function staffs()
    {
        return $this->belongsToMany(Staff::class);
    }
}