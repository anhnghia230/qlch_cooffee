<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Drinks extends Model
{
    use HasFactory;

    protected $fillable = [
        "typeDrinks_id",
        "name",
        "price",

    ];

    public function typeDrinks()
    {
        return $this->belongsTo(TypeDrinks::class);
    }
}