<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TypeDrinks extends Model
{
    use HasFactory;

    protected $fillable = [
        "name",
    ];

    public function drinkss()
    {
        return $this->hasMany(Drinks::class);
    }
}