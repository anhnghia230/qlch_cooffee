<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    use HasFactory;

    protected $fillable = [
        "user_id",
        "shop_id",
        "name",
        "age",
        "address",
        "phone",
        "cmnd",
        "workingDay",
        "position",
        "shift",
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function shop()
    {
        return $this->belongsTo(Shop::class);
    }

    public function timekeepings()
    {
        return $this->belongsToMany(Timekeeping::class);
    }

    public function revenues()
    {
        return $this->belongsToMany(Revenue::class);
    }
}