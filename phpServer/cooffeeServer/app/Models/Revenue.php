<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Revenue extends Model
{
    use HasFactory;

    protected $fillable = [
        "staff_id",
        "name",
        "orderDetails",
        "price",
        "date",

    ];

    public function staffs()
    {
        return $this->belongsToMany(Staff::class);
    }
}