<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StaffRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "user_id"=>"required|integer",
            "shop_id"=>"required|integer",
            "name"=>"required|string",
            "age"=>"required|string",
            "address"=>"required|string",
            "phone"=>"required|string|between:10,11",
            "cmnd"=>"required|string|min:15",
            "workingDay"=>"required|date",
            "position"=>"required|string",
            "shift"=>"required|string",
        ];
    }
}