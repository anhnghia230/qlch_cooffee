<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\AuthRequest;
use App\Mail\RegisterSuccess;
use App\Models\User;
use Illuminate\Support\Facades\Mail;

class AuthController extends Controller
{
     /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:api', ['except' => ['login', 'register']]);
      
    }

    public function login(AuthRequest $request) 
    {
        if (! $token = auth()->attempt($request->all())) {
                return response()->json(['error' => 'Unauthorized'], 401);
        }
        
        return  $this->createNewToken($token);
     

        

    }

    public function register(AuthRequest $request) {
        $user = User::create(array_merge(
            $request->all(),
            ['password' => bcrypt($request->password)]
        ));
        return response()->json([
            Mail::to("$request->email")->send(new RegisterSuccess($user)),
            'message' => 'User successfully registered',
            'user' => $user
        ], 201);

    }

    public function logout() {
        auth('api')->logout();
 
        return response()->json(['message' => 'User successfully signed out']);
    }

    public function refresh() {
        return $this->createNewToken(auth('api')->refresh());
    }

    public function userProfile() {
        return response()->json(auth('api')->user());
    }

    protected function createNewToken($token){
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60,
            'user' =>auth('api')->user()
        ]);
    }

    public function changePassWord(AuthRequest $request ) {
        $userId =auth('api')->user()->id;
 
        $user = User::where('id', $userId)->update(
                    ['password' => bcrypt($request->new_password)]
                );
 
        return response()->json([
            'message' => 'User successfully changed password',
            'user' => $user,
        ], 201);
    }


 

}