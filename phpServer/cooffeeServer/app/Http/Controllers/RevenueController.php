<?php

namespace App\Http\Controllers;

use App\Http\Requests\RevenueRequest;
use App\Models\Revenue;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RevenueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Revenue = Revenue::all();
        return view("admin/Revenue/revenue",compact("Revenue"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(RevenueRequest $request)
    {
        $Revenue = Revenue::create($request->all());
        return redirect()->action([RevenueController::class,"index"]);
    }

    public function totalPrice()
    {
        $Revenue = Revenue::select("staff_id",DB::raw("sum(price) as totalPrice"),DB::raw("count(staff_id) as staff_idCount"))->groupBy("staff_id")->get();
        return $Revenue;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Revenue  $revenue
     * @return \Illuminate\Http\Response
     */
    public function show(Revenue $revenue)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Revenue  $revenue
     * @return \Illuminate\Http\Response
     */
    public function edit( $id)
    {
        $Revenue = Revenue::find($id);
        return view("admin/Revenue/updateRevenue",compact("Revenue"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Revenue  $revenue
     * @return \Illuminate\Http\Response
     */
    public function update(RevenueRequest $request,  $id)
    {
        $Revenue=Revenue::find($id);
        $Revenue->update($request->all());
        return redirect()->action([RevenueController::class,"index"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Revenue  $revenue
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id)
    {
        $Revenue = Revenue::find($id);
        $Revenue->delete();
        return redirect()->action([RevenueController::class,"index"]);
    }
}