<?php

namespace App\Http\Controllers;

use App\Http\Requests\StaffRequest;
use App\Models\Staff;
use Illuminate\Http\Request;

class StaffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $Staff = Staff::all();

      return view("/admin/Staff/staff",compact("Staff"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(StaffRequest $request)
    {
      
        $Staff = Staff::create($request->all());

        return redirect()->action([StaffController::class,"index"]);
      

    }

    public function search( $name ) 
    {
    
        $Staff = Staff::where("name","like","%$name%")->orderBy("age","asc")->get();

        return $Staff;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Staff  $staff
     * @return \Illuminate\Http\Response
     */
    public function show(Staff $staff)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Staff  $staff
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Staff = Staff::find($id);
        return view("admin/Staff/updateStaff",compact("Staff"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Staff  $staff
     * @return \Illuminate\Http\Response
     */
    public function update(StaffRequest $request, $id)
    {
        $Staff = Staff::find($id);
        $Staff->update($request->all());
        return redirect()->action([StaffController::class,"index"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Staff  $staff
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Staff = Staff::find($id);
        $Staff->delete();
        return redirect()->action([StaffController::class,"index"]);
    }
}