<?php

namespace App\Http\Controllers;

use App\Http\Requests\TimeKeepingRequest;
use App\Models\Timekeeping;
use Illuminate\Http\Request;

class TimekeepingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Timekeeping = Timekeeping::all();
        return view("admin/TimeKeeping/timeKeeping",compact("Timekeeping"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(TimeKeepingRequest $request)
    {
        $TimeKeeping = Timekeeping::create($request->all());
        return redirect()->action([TimekeepingController::class,"index"]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Timekeeping  $timekeeping
     * @return \Illuminate\Http\Response
     */
    public function show(Timekeeping $timekeeping)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Timekeeping  $timekeeping
     * @return \Illuminate\Http\Response
     */
    public function edit( $id)
    {
        $TimeKeeping = Timekeeping::find($id);
        return view("admin/TimeKeeping/updateTimeKeeping",compact("TimeKeeping"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Timekeeping  $timekeeping
     * @return \Illuminate\Http\Response
     */
    public function update(TimeKeepingRequest $request,  $id)
    {
        $TimeKeeping = Timekeeping::find($id);
        $TimeKeeping->update($request->all());
        return redirect()->action([TimekeepingController::class,"index"]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Timekeeping  $timekeeping
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id)
    {
        $TimeKeeping=Timekeeping::find($id);
        $TimeKeeping->delete();      
        return redirect()->action([TimekeepingController::class,"index"]);

    }
}