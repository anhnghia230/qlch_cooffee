<?php

namespace App\Http\Controllers;

use App\Http\Requests\TypeDrinksRequest;
use App\Models\TypeDrinks;
use Illuminate\Http\Request;

class TypeDrinksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $TypeDrinks = TypeDrinks::all();
        return view("admin/TypeDrinks/typeDrinks",compact("TypeDrinks"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(TypeDrinksRequest $request)
    {
        $TypeDrinks=TypeDrinks::create($request->all());
        return redirect()->action([TypeDrinksController::class,"index"]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TypeDrinks  $typeDrinks
     * @return \Illuminate\Http\Response
     */
    public function show(TypeDrinks $typeDrinks)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TypeDrinks  $typeDrinks
     * @return \Illuminate\Http\Response
     */
    public function edit( $id)
    {
        $TypeDrinks = TypeDrinks::find($id);
        return view("admin/TypeDrinks/updateTypeDrinks",compact("TypeDrinks")); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TypeDrinks  $typeDrinks
     * @return \Illuminate\Http\Response
     */
    public function update(TypeDrinksRequest $request,$id)
    {
        $TypeDrinks = TypeDrinks::find($id);
        $TypeDrinks->update($request->all());
        return redirect()->action([TypeDrinksController::class,"index"]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TypeDrinks  $typeDrinks
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $TypeDrinks = TypeDrinks::find($id);
        $TypeDrinks->delete();
        return redirect()->action([TypeDrinksController::class,"index"]);
    }
}