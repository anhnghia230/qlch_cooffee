<?php

namespace App\Http\Controllers;

use App\Http\Requests\DrinksRequest;
use App\Models\Drinks;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DrinksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Drinks=Drinks::all();
        return view("admin/Drinks/drinks",compact("Drinks"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(DrinksRequest $request)
    {
        $Drinks = Drinks::create($request->all());
        return redirect()->action([DrinksController::class,"index"]);
    }


    public function groupDrinks (Request $request)
    {
       $Drinks = Drinks::select("typeDrinks_id",DB::raw("count(price) as totalPrice"))->groupBy("typeDrinks_id")->get();
      return $Drinks;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Drinks  $drinks
     * @return \Illuminate\Http\Response
     */
    public function show(Drinks $drinks)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Drinks  $drinks
     * @return \Illuminate\Http\Response
     */
    public function edit( $id)
    {
        $Drinks = Drinks::find($id);
        return view("admin/Drinks/updateDrinks",compact("Drinks"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Drinks  $drinks
     * @return \Illuminate\Http\Response
     */
    public function update(DrinksRequest $request, $id)
    {
        $Drinks = Drinks::find($id);
        $Drinks->update($request->all());  
        
        return redirect()->action([DrinksController::class,"index"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Drinks  $drinks
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Drinks = Drinks::find($id);
        $Drinks->delete();
        
        return redirect()->action([DrinksController::class,"index"]);
    }
}