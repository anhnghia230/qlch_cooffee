<?php

namespace App\Providers;

use Illuminate\Support\Facades\Response;
use Illuminate\Support\ServiceProvider;

class SuccessProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
    //     $response->macro("success",function($data){
    //         [
    //             "success"=>"SuccessFully!",
    //             "data"=>$data,
    //         ]
    //     });
    }
}