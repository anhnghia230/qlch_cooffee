<?php

use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\DrinksController;
use App\Http\Controllers\RevenueController;
use App\Http\Controllers\ShopController;
use App\Http\Controllers\StaffController;
use App\Http\Controllers\TimekeepingController;
use App\Http\Controllers\TypeDrinksController;
use App\Http\Controllers\UploadFileController;
use App\Http\Middleware\CheckLogin;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get("/staff",[StaffController::class,"index"]);

Route::get("/login",function()
{
    return view("/admin/login");
});
Route::post("/login/admin",[AuthController::class,"login"]);


    // upload-file
    
    Route::get("upload-file",[UploadFileController::class,"index"]);
    Route::post("upload/file",[UploadFileController::class,"uploadFile"]);
    
Route::middleware("CheckLogin")->prefix("admin")->group(function()
{
    Route::get("/",function()
    {
        return view("/admin/admin");
    });

//handle staff

    Route::get("staff",[StaffController::class,"index"]);
    Route::get("add/staff",function(){return view("admin/Staff/addStaff");});
    Route::get("update/staff/{id}",[StaffController::class,"edit"]);


// handle shop
    Route::get("shop",[ShopController::class,"index"]);
    Route::get("add/shop",function(){return view("admin/Shop/addShop");});
    Route::get("update/shop/{id}",[ShopController::class,"edit"]);


// handle drinks
    Route::get("drinks",[DrinksController::class,"index"]);
    Route::get("add/drinks",function(){return view("admin/Drinks/addDrinks");});
    Route::get("update/drinks/{id}",[DrinksController::class,"edit"]);



// handle TypeDrinks

    Route::get("typeDrinks",[TypeDrinksController::class,"index"]);
    Route::get("add/typeDrinks",function(){return view("admin/TypeDrinks/addTypeDrinks");});
    Route::get("update/typeDrinks/{id}",[TypeDrinksController::class,"edit"]);



// handle TimeKeeping
    Route::get("timekeeping",[TimekeepingController::class,"index"]);
    Route::get("add/timeKeeping",function(){return view("admin/TimeKeeping/addTimeKeeping");});
    Route::get("update/timeKeeping/{id}",[TimekeepingController::class,"edit"]);




// handle Revenue


    Route::get("revenue",[RevenueController::class,"index"]);
    Route::get("add/revenue",function(){return view("admin/Revenue/addRevenue");});
    Route::get("update/revenue/{id}",[RevenueController::class,"edit"]);
});