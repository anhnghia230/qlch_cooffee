<?php

use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\DrinksController;
use App\Http\Controllers\RevenueController;
use App\Http\Controllers\ShopController;
use App\Http\Controllers\StaffController;
use App\Http\Controllers\TimekeepingController;
use App\Http\Controllers\TypeDrinksController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'
 
], function () {
    
    Route::post('/login', [AuthController::class,"login"]);
    Route::post('/register',[AuthController::class,"register"]);
    Route::post('/logout', [AuthController::class,"logout"]);
    Route::post('/refresh', [AuthController::class,"refresh"]);
    Route::get('/user-profile',[AuthController::class,"userProfile"]);
    Route::post('/change-pass', [AuthController::class,"changePassWord"]);   
});

Route::group([
    "middleware"=>"api",
],function()
{
    Route::get("/staff",[StaffController::class,"index"]);
    Route::post("/add/staff",[StaffController::class,"create"]);
    Route::put("/update/staff/{id}",[StaffController::class,"update"]);
    Route::delete("/delete/staff/{id}",[StaffController::class,"destroy"]);
    Route::get("/search/{name}",[StaffController::class,"search"]);
});

Route::group([
    "middleware"=>"api",
],function()
{
    Route::get("/shop",[ShopController::class,"index"]);
    Route::post("/add/shop",[ShopController::class,"create"]);
    Route::put("/update/shop/{id}",[ShopController::class,"update"]);
    Route::delete("/delete/shop/{id}",[ShopController::class,"destroy"]);
});

Route::group([
    "middleware"=>"api",
],function()
{
    Route::get("/drinks",[DrinksController::class,"index"]);
    Route::post("/add/drinks",[DrinksController::class,"create"]);
    Route::put("/update/drinks/{id}",[DrinksController::class,"update"]);
    Route::delete("/delete/drinks/{id}",[DrinksController::class,"destroy"]);
    Route::get("/group",[DrinksController::class,"groupDrinks"]);
});

Route::group([
    "middleware"=>"api",
],function()
{
    Route::get("/typeDrinks",[TypeDrinksController::class,"index"]);
    Route::post("/add/typeDrinks",[TypeDrinksController::class,"create"]);
    Route::put("/update/typeDrinks/{id}",[TypeDrinksController::class,"update"]);
    Route::delete("/delete/typeDrinks/{id}",[TypeDrinksController::class,"destroy"]);
});

Route::group([
    "middleware"=>"api",
],function()
{
    Route::get("/timeKeeping",[TimekeepingController::class,"index"]);
    Route::post("/add/timeKeeping",[TimekeepingController::class,"create"]);
    Route::put("/update/timeKeeping/{id}",[TimekeepingController::class,"update"]);
    Route::delete("/delete/timeKeeping/{id}",[TimekeepingController::class,"destroy"]);
});

Route::group([
    "middleware"=>"api",
],function()
{
    Route::get("/revenue",[RevenueController::class,"index"]);
    Route::post("/add/revenue",[RevenueController::class,"create"]);
    Route::put("/update/revenue/{id}",[RevenueController::class,"update"]);
    Route::delete("/delete/revenue/{id}",[RevenueController::class,"destroy"]);
    Route::get("/groupTotalPrice",[RevenueController::class,"totalPrice"]);
});