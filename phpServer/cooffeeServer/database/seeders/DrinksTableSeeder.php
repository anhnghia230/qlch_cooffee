<?php

namespace Database\Seeders;

use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DrinksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fake = Factory::create();
        $limit = 50;
        for($i = 0 ; $i<$limit ;$i++)
        {
            DB::table("drinks")->insert([
                "typeDrinks_id"=>$fake->numberBetween($min=1,$max=20),
                "name"=>$fake->name,
                "price"=>$fake->numberBetween($min =20, $max =60),
                "created_at"=>$fake->date("Y-m-d H:i:s"),
                "updated_at"=>$fake->date("Y-m-d H:i:s"),
            ]);
        }
    }
}