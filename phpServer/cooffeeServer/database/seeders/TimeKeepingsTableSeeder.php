<?php

namespace Database\Seeders;

use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TimeKeepingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fake = Factory::create();
        $limit = 200;
        for($i=0;$i<$limit;$i++)
        {
            DB::table("timekeepings")->insert([
                "staff_id"=>$fake->numberBetween($min=1,$max=50),
                "Checkin"=>$fake->date("Y-m-d H:i:s"),
                "Checkout"=>$fake->date("Y-m-d H:i:s"),
                "created_at"=>$fake->date("Y-m-d H:i:s"),
                "updated_at"=>$fake->date("Y-m-d H:i:s"),
            ]);
        }
    }
}