<?php

namespace Database\Seeders;

use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RevenuesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fake = Factory::create();
        $limit = 200;
        for($i =0;$i<$limit;$i++)
        {
            DB::table("revenues")->insert([
                "staff_id"=>$fake->numberBetween($min=1,$max=50),
                "name"=>$fake->name,
                "orderDetails"=>$fake->sentence(10),
                "date"=>$fake->date("Y-m-d H:i:s"),
                "created_at"=>$fake->date("Y-m-d H:i:s"),
                "updated_at"=>$fake->date("Y-m-d H:i:s"),
            ]);
        }
    }
}