<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
       $this->call(StaffTableSeeder::class);
       $this->call(ShopTableSeeder::class);
       $this->call(DrinksTableSeeder::class);
       $this->call(TypeDrinksTableSeeder::class);
       $this->call(TimeKeepingsTableSeeder::class);
       $this->call(RevenuesTableSeeder::class);
    }
}