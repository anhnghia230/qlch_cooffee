<?php

namespace Database\Seeders;

use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ShopTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fake = Factory::create();
        $limit = 10;
        for($i=0;$i<$limit;$i++)
        {
            DB::table("shops")->insert([
                "user_id"=>1,
                "name"=>$fake->name,
                "address"=>$fake->name,
                "countStaff"=>$fake->numberBetween($min=7,$max=10),
                "created_at"=>$fake->date("Y-m-d H:i:s"),
                "updated_at"=>$fake->date("Y-m-d H:i:s"),
            ]);
        }
    }
}