<?php

namespace Database\Seeders;

use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StaffTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fake = Factory::create();
        $limit=50;
       for($i =0 ;$i<$limit;$i++)
       {
         DB::table("staff")->insert([
            "user_id"=>1,
            "shop_id"=>$fake->numberBetween($min=1,$max=10),
            "name"=>$fake->name,
            "age"=>$fake->numberBetween($min = 18, $max = 30),
            "address"=>$fake->city,
            "phone"=>$fake->phoneNumber,
            "cmnd"=>$fake->numberBetween($min = 1000,$max = 10000),
            "workingDay"=>$fake->date("Y-m-d H:i:s"),
            "position"=>$fake->name,
            "shift"=>$fake->sentence(10),
            "created_at"=>$fake->date("Y-m-d H:i:s"),
            "updated_at"=>$fake->date("Y-m-d H:i:s"),
         ]);
       }
    }
}