<?php

namespace Database\Seeders;

use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TypeDrinksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fake = Factory::create();
        $limit = 20;
        for($i=0;$i<$limit;$i++)
        {
            DB::table("type_drinks")->insert([
                "name"=>$fake->name,
                "created_at"=>$fake->date("Y-m-d H:i:s"),
                "updated_at"=>$fake->date("Y-m-d H:i:s"),
            ]);
        }
    }
}