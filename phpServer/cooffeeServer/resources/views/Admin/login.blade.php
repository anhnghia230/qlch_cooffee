<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>login</title>
</head>
<style>
h1 {
    margin-top: 100px;
    display: flex;
    justify-content: center;
    align-items: center;
}

.form {
    width: 600px;
    margin: 0 auto;

}

.form-login {
    width: 300px;
    display: flex;
    flex-direction: column;
    margin: 0 auto;
}

.form-input {
    padding: 8px 8px;
    font-size: 16px;
    margin: 10px 0;
}

.form-btn {
    width: 140px;
    padding: 8px 8px;
    border-radius: 2px;
    border: 1px solid #ccc;
    background-color: transparent;
    cursor: pointer;
}

.form-btn:hover {
    background-color: #ccc;
    color: white;
}
</style>

<body>

    <h1>Login</h1>
    <form action="/login/admin" method="post" class="form">
        @csrf
        <div class="form-login">
            <label>Email:</label>
            <input class="form-input" type="email" required name="email" placeholder="Nhập email...">
            <label>Password:</label>
            <input class="form-input" type="password" required name="password" placeholder="Nhập password...">
            <button class="form-btn" type="submit">Login</button>
        </div>
    </form>

</body>

</html>