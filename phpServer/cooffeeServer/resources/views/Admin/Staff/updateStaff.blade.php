<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<style>
* {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
}

.staff {
    display: flex;
}

.staff-add {
    margin-left: 20px;
    margin-top: 20px;
}

.staff-form {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    margin-bottom: 10px;
}

.staff-label-input {
    display: flex;
    flex-direction: column;
    margin-top: 10px;

}

.staff-label-input label {
    margin-bottom: 5px;
}

.staff-input {
    padding: 6px 10px;
    font-size: 16px;
    border-radius: 4px;
    margin-left: 20px;
}

.staff-btn {
    padding: 6px 10px;
    margin-top: 10px;
    border-radius: 4px;
}
</style>

<body>
    @include("Admin.Layout.header")
    <div class="staff">
        @include("Admin.Layout.navbar")
        <div class="staff-add">
            <h2>Add staff</h2>
            <form class="staff-form" action="/api/update/staff/{{$Staff->id}}" method="post">
                @method("PUT")
                @csrf
                <div class="staff-label-input">
                    <label for="user_id">user_id:</label>
                    <input class="staff-input" type="text" required name="user_id" value="{{$Staff->user_id}}"
                        placeholder="nhập user_id...">
                </div>
                <div class="staff-label-input">
                    <label for="shop_id">shop_id:</label>
                    <input class="staff-input" type="text" required value="{{$Staff->shop_id}}" name="shop_id"
                        placeholder="nhập shop_id...">
                </div>
                <div class="staff-label-input">
                    <label for="name">name:</label>
                    <input class="staff-input" type="text" required value="{{$Staff->name}}" name="name"
                        placeholder="nhập name...">
                </div>
                <div class="staff-label-input">
                    <label for="age">age:</label>
                    <input class="staff-input" type="text" required value="{{$Staff->age}}" name="age"
                        placeholder="nhập age...">
                </div>
                <div class="staff-label-input">
                    <label for="address">address:</label>
                    <input class="staff-input" type="text" required value="{{$Staff->address}}" name="address"
                        placeholder="nhập address...">
                </div>
                <div class="staff-label-input">
                    <label for="phone">phone:</label>
                    <input class="staff-input" type="text" required value="{{$Staff->phone}}" name="phone"
                        placeholder="nhập phone...">
                </div>
                <div class="staff-label-input">
                    <label for="cmnd">cmnd:</label>
                    <input class="staff-input" type="text" required value="{{$Staff->cmnd}}" name="cmnd"
                        placeholder="nhập cmnd...">
                </div>
                <div class="staff-label-input">
                    <label for="workingDay">workingDay:</label>
                    <input class="staff-input" type="text" required value="{{$Staff->workingDay}}" name="workingDay"
                        placeholder="nhập workingDay...">
                </div>
                <div class="staff-label-input">
                    <label for="position">position:</label>
                    <input class="staff-input" type="text" required value="{{$Staff->position}}" name="position"
                        placeholder="nhập position...">
                </div>
                <div class="staff-label-input">
                    <label for="shift">shift:</label>
                    <input class="staff-input" type="text" required value="{{$Staff->shift}}" name="shift"
                        placeholder="nhập shift...">
                </div>
                <button class="staff-btn" type="submit">
                    submit
                </button>

            </form>
        </div>
    </div>
    @include("Admin.Layout.footer")
</body>

</html>