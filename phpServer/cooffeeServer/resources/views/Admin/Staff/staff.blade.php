<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<style>
* {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
}

.staff {
    display: flex;
}

.staff-table {
    margin-top: 20px;
    margin-left: 20px;
}

.staff-table-header {
    margin-top: 20px;
    display: grid;
    grid-template-columns: repeat(11, 100px);
    gap: 20px;
    text-align: center;
    justify-content: center;
    align-items: center;
}

.add {
    display: flex;
    align-items: center;
}

.add a {
    margin-left: 20px;
    border: 1px solid #ccc;
    padding: 6px 6px;
    border-radius: 6px;
}
</style>

<body>
    @include("Admin.Layout.header")
    <div class="staff">
        @include("Admin.Layout.navbar")
        <div class="staff-table">
            <div class="add">
                <h2> staff</h2>
                <a href="/admin/add/staff">Add Staff</a>
            </div>
            <div>
                <table>
                    <tr class="staff-table-header">
                        <th> user_id
                        </th>
                        <th>
                            shop_id
                        </th>
                        <th>
                            name
                        </th>
                        <th>
                            age
                        </th>
                        <th>
                            address
                        </th>
                        <th>
                            phone
                        </th>
                        <th>
                            cmnd
                        </th>
                        <th>
                            workingDay
                        </th>
                        <th>
                            position
                        </th>
                        <th>
                            shift
                        </th>
                        <th>handle</th>
                    </tr>
                    @foreach($Staff as $Staffs)
                    <tr class="staff-table-header">
                        <td>
                            {{$Staffs->user_id}}
                        </td>
                        <td>
                            {{$Staffs->shop_id}}
                        </td>
                        <td>
                            {{$Staffs->name}}
                        </td>
                        <td>
                            {{$Staffs->age}}
                        </td>
                        <td>
                            {{$Staffs->address}}
                        </td>
                        <td>
                            {{$Staffs->phone}}
                        </td>
                        <td>
                            {{$Staffs->cmnd}}
                        </td>
                        <td>
                            {{$Staffs->workingDay}}
                        </td>
                        <td>
                            {{$Staffs->position}}
                        </td>
                        <td>
                            {{$Staffs->shift}}
                        </td>
                        <td><a href="/admin/update/staff/{{$Staffs->id}}">edit</a>
                            <form action="/api/delete/staff/{{$Staffs->id}}" method="post">
                                @csrf
                                @method("DELETE")
                                <button type="submit">delete</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </table>
            </div>

        </div>
    </div>
    @include("Admin.Layout.footer")
</body>

</html>