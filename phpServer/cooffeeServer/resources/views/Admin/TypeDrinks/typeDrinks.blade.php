<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<style>
* {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
}

.typeDrinks {
    display: flex;
}

.typeDrinks-table {
    margin-top: 20px;
    margin-left: 20px;
}

.typeDrinks-table-header {
    margin-top: 20px;
    display: grid;
    grid-template-columns: repeat(5, 100px);
    gap: 20px;
    text-align: center;
    justify-content: center;
    align-items: center;

}

.add {
    display: flex;
    align-items: center;
}

.add a {
    margin-left: 20px;
    border: 1px solid #ccc;
    padding: 6px 6px;
    border-radius: 6px;
}
</style>

<body>
    @include("Admin.Layout.header")
    <div class="typeDrinks">
        @include("Admin.Layout.navbar")
        <div class="typeDrinks-table">
            <div class="add">
                <h2> typeDrinks</h2>
                <a href="/admin/add/typeDrinks">Add TypeDrinks</a>
            </div>
            <div>
                <table>
                    <tr class="typeDrinks-table-header">
                        <th>
                            name
                        </th>

                    </tr>
                    @foreach($TypeDrinks as $TypeDrinkss)
                    <tr class="typeDrinks-table-header">

                        <td>
                            {{$TypeDrinkss->name}}
                        </td>


                        <td><a href="/admin/update/typeDrinks/{{$TypeDrinkss->id}}">edit</a>
                            <form action="/api/delete/typeDrinks/{{$TypeDrinkss->id}}" method="post">
                                @csrf
                                @method("DELETE")
                                <button type="submit">delete</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </table>
            </div>

        </div>
    </div>
    @include("Admin.Layout.footer")
</body>

</html>