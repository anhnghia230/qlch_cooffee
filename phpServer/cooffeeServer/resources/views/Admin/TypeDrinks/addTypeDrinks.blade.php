<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<style>
* {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
}

.typeDrinks {
    display: flex;
}

.typeDrinks-add {
    margin-left: 20px;
    margin-top: 20px;
}

.typeDrinks-form {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
}

.typeDrinks-label-input {
    display: flex;
    flex-direction: column;
    margin-top: 10px;

}

.typeDrinks-label-input label {
    margin-bottom: 5px;
}

.typeDrinks-input {
    padding: 6px 10px;
    font-size: 16px;
    border-radius: 4px;
    margin-left: 20px;
}

.typeDrinks-btn {
    padding: 6px 10px;
    margin-top: 10px;
    border-radius: 4px;
}
</style>

<body>
    @include("Admin.Layout.header")
    <div class="typeDrinks">
        @include("Admin.Layout.navbar")
        <div class="typeDrinks-add">
            <h2>Add TypeDrinks</h2>
            <form class="typeDrinks-form" action="/api/add/typeDrinks" method="post">
                @csrf

                <div class="typeDrinks-label-input">
                    <label for="name">name:</label>
                    <input class="typeDrinks-input" type="text" required name="name" placeholder="nhập name...">
                </div>

                <button class="typeDrinks-btn" type="submit">
                    submit
                </button>

            </form>
        </div>
    </div>
    @include("Admin.Layout.footer")
</body>

</html>