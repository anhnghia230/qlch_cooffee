<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<style>
* {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
}

.timeKeeping {
    display: flex;
}

.timeKeeping-table {
    margin-top: 20px;
    margin-left: 20px;
}

.timeKeeping-table-header {
    margin-top: 20px;
    display: grid;
    grid-template-columns: repeat(5, 100px);
    gap: 20px;
    text-align: center;
    justify-content: center;
    align-items: center;
}

.add {
    display: flex;
    align-items: center;
}

.add a {
    margin-left: 20px;
    border: 1px solid #ccc;
    padding: 6px 6px;
    border-radius: 6px;
}
</style>

<body>
    @include("Admin.Layout.header")
    <div class="timeKeeping">
        @include("Admin.Layout.navbar")
        <div class="timeKeeping-table">
            <div class="add">
                <h2> timeKeeping</h2>
                <a href="/admin/add/timeKeeping">Add TimeKeeping</a>
            </div>
            <div>
                <table>
                    <tr class="timeKeeping-table-header">
                        <th> staff_id
                        </th>
                        <th>
                            Checkin
                        </th>
                        <th>
                            Checkout
                        </th>

                        <th>handle</th>
                    </tr>
                    @foreach($Timekeeping as $Timekeepings)

                    <tr class="timeKeeping-table-header">
                        <td>
                            {{$Timekeepings->staff_id}}
                        </td>
                        <td>
                            {{$Timekeepings->Checkin}}
                        </td>
                        <td>
                            {{$Timekeepings->Checkout}}
                        </td>


                        <td><a href="/admin/update/timeKeeping/{{$Timekeepings->id}}">edit</a>
                            <form action="/api/delete/timeKeeping/{{$Timekeepings->id}}" method="post">
                                @csrf
                                @method("DELETE")
                                <button type="submit">delete</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </table>
            </div>

        </div>
    </div>
    @include("Admin.Layout.footer")
</body>

</html>