<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<style>
* {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
}

.timeKeeping {
    display: flex;
}

.timeKeeping-add {
    margin-left: 20px;
    margin-top: 20px;
}

.timeKeeping-form {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
}

.timeKeeping-label-input {
    display: flex;
    flex-direction: column;
    margin-top: 10px;

}

.timeKeeping-label-input label {
    margin-bottom: 5px;
}

.timeKeeping-input {
    padding: 6px 10px;
    font-size: 16px;
    border-radius: 4px;
    margin-left: 20px;
}

.timeKeeping-btn {
    padding: 6px 10px;
    margin-top: 10px;
    border-radius: 4px;
}
</style>

<body>
    @include("Admin.Layout.header")
    <div class="timeKeeping">
        @include("Admin.Layout.navbar")
        <div class="timeKeeping-add">
            <h2>Add timeKeeping</h2>
            <form class="timeKeeping-form" action="/api/update/timeKeeping/{{$TimeKeeping->id}}" method="post">
                @method("PUT")
                @csrf
                <div class="timeKeeping-label-input">
                    <label for="staff_id">staff_id:</label>
                    <input class="timeKeeping-input" type="text" value="{{$TimeKeeping->staff_id}}" required
                        name="staff_id" placeholder="nhập staff_id...">
                </div>
                <div class="timeKeeping-label-input">
                    <label for="Checkin">Checkin:</label>
                    <input class="timeKeeping-input" type="text" value="{{$TimeKeeping->Checkin}}" required
                        name="Checkin" placeholder="nhập Checkin...">
                </div>
                <div class="timeKeeping-label-input">
                    <label for="Checkout">Checkout:</label>
                    <input class="timeKeeping-input" type="text" value="{{$TimeKeeping->Checkout}}" required
                        name="Checkout" placeholder="nhập Checkout...">
                </div>
                <button class="timeKeeping-btn" type="submit">
                    submit
                </button>

            </form>
        </div>
    </div>
    @include("Admin.Layout.footer")
</body>

</html>