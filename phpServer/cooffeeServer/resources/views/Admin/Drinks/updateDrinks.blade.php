<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<style>
* {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
}

.drinks {
    display: flex;
}

.drinks-add {
    margin-left: 20px;
    margin-top: 20px;
}

.drinks-form {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
}

.drinks-label-input {
    display: flex;
    flex-direction: column;
    margin-top: 10px;

}

.drinks-label-input label {
    margin-bottom: 5px;
}

.drinks-input {
    padding: 6px 10px;
    font-size: 16px;
    border-radius: 4px;
    margin-left: 20px;
}

.drinks-btn {
    padding: 6px 10px;
    margin-top: 10px;
    border-radius: 4px;
}
</style>

<body>
    @include("Admin.Layout.header")
    <div class="drinks">
        @include("Admin.Layout.navbar")
        <div class="drinks-add">
            <h2>Add drinks</h2>
            <form class="drinks-form" action="/api/update/drinks/{{$Drinks->id}}" method="post">
                @method("PUT")
                @csrf
                <div class="drinks-label-input">
                    <label for="typeDrinks_id">typeDrinks_id:</label>
                    <input class="drinks-input" type="text" value="{{$Drinks->typeDrinks_id}}" required
                        name="typeDrinks_id" placeholder="nhập typeDrinks_id...">
                </div>
                <div class="drinks-label-input">
                    <label for="name">name:</label>
                    <input class="drinks-input" type="text" value="{{$Drinks->name}}" required name="name"
                        placeholder="nhập name...">
                </div>
                <div class="drinks-label-input">
                    <label for="price">price:</label>
                    <input class="drinks-input" type="text" value="{{$Drinks->price}}" required name="price"
                        placeholder="nhập price...">
                </div>
                <button class="drinks-btn" type="submit">
                    submit
                </button>

            </form>
        </div>
    </div>
    @include("Admin.Layout.footer")
</body>

</html>