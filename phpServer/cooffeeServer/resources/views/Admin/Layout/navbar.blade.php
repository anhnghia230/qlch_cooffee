<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<style>
.admin-nav {
    width: 400px;
    height: 600px;
    border-right: 1px solid #ccc;
}

.nav-header {
    width: 100%;
    height: 40px;
    font-size: 20px;
    display: flex;
    align-items: center;
    padding-left: 20px;
    border-bottom: 1px solid #ccc;
}

a {

    text-decoration: none;
    color: black;
}

.nav-name {
    font-size: 16px;
    padding: 10px 10px 10px 20px;
    margin-top: 10px;
    cursor: pointer;

}
</style>

<body>
    <nav class="admin-nav">
        <div>
            <h1 class="nav-header"> Product DashBoard</h1>
        </div>
        <div>
            <a href="/admin/staff">
                <p class="nav-name">Staff</p>
            </a>
            <a href="/admin/shop">
                <p class="nav-name">Shop</p>
            </a>
            <a href="/admin/drinks">
                <p class="nav-name">Drinks</p>
            </a>
            <a href="/admin/typeDrinks">
                <p class="nav-name">TypeDrinks</p>
            </a>
            <a href="/admin/timekeeping">
                <p class="nav-name">TimeKeeping</p>
            </a>
            <a href="/admin/revenue">
                <p class="nav-name">Revenue</p>
            </a>
        </div>
    </nav>
</body>

</html>