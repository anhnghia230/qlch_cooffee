<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<style>
.admin-header {
    width: 100%;
    height: 80px;
    border-bottom: 1px solid #ccc;
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 0 80px;

}

.header-logo {
    font-size: 40px;
    font-weight: bold;
    color: black;
    margin: 0;
}

.header-account {
    display: flex;
    justify-content: center;
    align-items: center;
    cursor: pointer;
    position: relative;
}

.header-account:hover .header-account-nav {
    display: block;
}

.header-account p {
    margin-left: 10px;
}

.header-account-nav {
    display: none;
    position: absolute;
    top: 40px;
    border: 1px solid #ccc;
    border-radius: 4px;
}

.header-account-nav::before {
    content: "";
    position: absolute;
    top: -30px;
    width: 100px;
    height: 40px;
}

.header-account-nav p {
    margin: 0;
    margin-top: 20px;
    padding: 6px 6px;
    width: 100px;
}

.header-account-nav p:hover {
    background-color: #ccc;

}
</style>

<body>
    <header class="admin-header">
        <div>
            <h1 class="header-logo">brand</h1>
        </div>
        <div class="header-account">
            <img src="#" alt="img">
            <p>name</p>
            <div class="header-account-nav">
                <p>option</p>
                <p>logout</p>
            </div>
        </div>
    </header>
</body>

</html>