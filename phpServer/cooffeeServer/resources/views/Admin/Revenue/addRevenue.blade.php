<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<style>
* {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
}

.revenue {
    display: flex;
}

.revenue-add {
    margin-left: 20px;
    margin-top: 20px;
}

.revenue-form {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
}

.revenue-label-input {
    display: flex;
    flex-direction: column;
    margin-top: 10px;

}

.revenue-label-input label {
    margin-bottom: 5px;
}

.revenue-input {
    padding: 6px 10px;
    font-size: 16px;
    border-radius: 4px;
    margin-left: 20px;
}

.revenue-btn {
    padding: 6px 10px;
    margin-top: 10px;
    border-radius: 4px;
}
</style>

<body>
    @include("Admin.Layout.header")
    <div class="revenue">
        @include("Admin.Layout.navbar")
        <div class="revenue-add">
            <h2>Add revenue</h2>
            <form class="revenue-form" action="/api/add/revenue" method="post">
                @csrf
                <div class="revenue-label-input">
                    <label for="staff_id">staff_id:</label>
                    <input class="revenue-input" type="text" required name="staff_id" placeholder="nhập staff_id...">
                </div>
                <div class="revenue-label-input">
                    <label for="name">name:</label>
                    <input class="revenue-input" type="text" required name="name" placeholder="nhập name...">
                </div>
                <div class="revenue-label-input">
                    <label for="orderDetails">orderDetails:</label>
                    <input class="revenue-input" type="text" required name="orderDetails"
                        placeholder="nhập orderDetails...">
                </div>
                <div class="revenue-label-input">
                    <label for="price">price:</label>
                    <input class="revenue-input" type="text" required name="price"
                        placeholder="nhập price...">
                </div>
                <div class="revenue-label-input">
                    <label for="date">date:</label>
                    <input class="revenue-input" type="text" required name="date" placeholder="nhập date...">
                </div>
                <button class="revenue-btn" type="submit">
                    submit
                </button>

            </form>
        </div>
    </div>
    @include("Admin.Layout.footer")
</body>

</html>