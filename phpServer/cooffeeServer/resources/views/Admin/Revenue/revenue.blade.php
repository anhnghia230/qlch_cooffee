<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<style>
* {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
}

.revenue {
    display: flex;
}

.revenue-table {
    margin-top: 20px;
    margin-left: 20px;
}

.revenue-table-header {
    margin-top: 20px;
    display: grid;
    grid-template-columns: repeat(5, 100px);
    gap: 20px;
    text-align: center;
    justify-content: center;
    align-items: center;
}

.add {
    display: flex;
    align-items: center;
}

.add a {
    margin-left: 20px;
    border: 1px solid #ccc;
    padding: 6px 6px;
    border-radius: 6px;
}
</style>

<body>
    @include("Admin.Layout.header")
    <div class="revenue">
        @include("Admin.Layout.navbar")
        <div class="revenue-table">
            <div class="add">
                <h2> revenue</h2>
                <a href="/admin/add/revenue">Add Revenue</a>
            </div>
            <div>
                <table>
                    <tr class="revenue-table-header">
                        <th> staff_id
                        </th>
                        <th>
                            name
                        </th>
                        <th>
                            orderDetails
                        </th>
                        <th>
                            price
                        </th>
                        <th>
                            date
                        </th>

                        <th>handle</th>
                    </tr>
                    @foreach($Revenue as $Revenues)

                    <tr class="revenue-table-header">
                        <td>
                            {{$Revenues->staff_id}}
                        </td>
                        <td>
                            {{$Revenues->name}}
                        </td>
                        <td>
                            {{$Revenues->orderDetails}}
                        </td>
                        <td>
                            {{$Revenues->price}}
                        </td>
                        <td>
                            {{$Revenues->date}}
                        </td>


                        <td><a href="/admin/update/revenue/{{$Revenues->id}}">edit</a>
                            <form action="/api/delete/revenue/{{$Revenues->id}}" method="post">
                                @csrf
                                @method("DELETE")
                                <button type="submit">delete</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </table>
            </div>

        </div>
    </div>
    @include("Admin.Layout.footer")
</body>

</html>