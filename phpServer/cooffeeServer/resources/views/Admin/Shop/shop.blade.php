<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<style>
* {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
}

.shop {
    display: flex;
}

.shop-table {
    margin-top: 20px;
    margin-left: 20px;
}

.shop-table-header {
    margin-top: 20px;
    display: grid;
    grid-template-columns: repeat(5, 100px);
    gap: 20px;
    text-align: center;
    justify-content: center;
    align-items: center;
}

.add {
    display: flex;
    align-items: center;
}

.add a {
    margin-left: 20px;
    border: 1px solid #ccc;
    padding: 6px 6px;
    border-radius: 6px;
}
</style>

<body>
    @include("Admin.Layout.header")
    <div class="shop">
        @include("Admin.Layout.navbar")
        <div class="shop-table">
            <div class="add">
                <h2> Shop</h2>
                <a href="/admin/add/shop">Add Shop</a>
            </div>
            <div>
                <table>
                    <tr class="shop-table-header">
                        <th> user_id
                        </th>
                        <th>
                            name
                        </th>
                        <th>
                            address
                        </th>
                        <th>
                            countshop
                        </th>
                        <th>handle</th>
                    </tr>
                    @foreach($Shop as $Shops)
                    <tr class="shop-table-header">
                        <td>
                            {{$Shops->user_id}}
                        </td>
                        <td>
                            {{$Shops->name}}
                        </td>
                        <td>
                            {{$Shops->address}}
                        </td>
                        <td>
                            {{$Shops->countStaff}}
                        </td>

                        <td><a href="/admin/update/shop/{{$Shops->id}}">edit</a>
                            <form action="/api/delete/shop/{{$Shops->id}}" method="post">
                                @csrf
                                @method("DELETE")
                                <button type="submit">delete</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </table>
            </div>

        </div>
    </div>
    @include("Admin.Layout.footer")
</body>

</html>