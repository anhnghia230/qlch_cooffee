<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<style>
* {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
}

.shop {
    display: flex;
}

.shop-add {
    margin-left: 20px;
    margin-top: 20px;
}

.shop-form {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
}

.shop-label-input {
    display: flex;
    flex-direction: column;
    margin-top: 10px;

}

.shop-label-input label {
    margin-bottom: 5px;
}

.shop-input {
    padding: 6px 10px;
    font-size: 16px;
    border-radius: 4px;
    margin-left: 20px;
}

.shop-btn {
    padding: 6px 10px;
    margin-top: 10px;
    border-radius: 4px;
}
</style>

<body>
    @include("Admin.Layout.header")
    <div class="shop">
        @include("Admin.Layout.navbar")
        <div class="shop-add">
            <h2>Add shop</h2>
            <form class="shop-form" action="/api/update/shop/{{$Shop->id}}" method="post">
                @method("PUT")
                @csrf
                <div class="shop-label-input">
                    <label for="user_id">user_id:</label>
                    <input class="shop-input" type="text" required name="user_id" value="{{$Shop->user_id}}"
                        placeholder="nhập user_id...">
                </div>
                <div class="shop-label-input">
                    <label for="name">name:</label>
                    <input class="shop-input" type="text" required name="name" value="{{$Shop->name}}"
                        placeholder="nhập name...">
                </div>
                <div class="shop-label-input">
                    <label for="address">address:</label>
                    <input class="shop-input" type="text" required name="address" value="{{$Shop->address}}"
                        placeholder="nhập address...">
                </div>
                <div class="shop-label-input">
                    <label for="countStaff">countStaff:</label>
                    <input class="shop-input" type="text" required name="countStaff" value="{{$Shop->countStaff}}"
                        placeholder="nhập countStaff...">
                </div>
                <button class="shop-btn" type="submit">
                    submit
                </button>

            </form>
        </div>
    </div>
    @include("Admin.Layout.footer")
</body>

</html>