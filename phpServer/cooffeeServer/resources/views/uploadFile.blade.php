<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<style>
img {
    width: 100px;
    height: 100px;
}
</style>

<body>
    <h1>upload-file</h1>
    <div>
        <h2>List files</h2>
        @foreach($UploadFile as $uploadFile)
        <div>
            <p>{{$uploadFile->file}}</p>
            <img src="{{asset($uploadFile->file)}}" alt="image">
        </div>
        @endforeach
    </div>
    <form class="form" action="/upload/file" method="post" enctype="multipart/form-data">
        @csrf
        <div>
            <label for="upload file"> Upload File</label>
        </div>
        <br>
        <div> <input type="file" name="file" required></div>
        <br>
        <button type="submit"> submit</button>
    </form>

</body>

</html>